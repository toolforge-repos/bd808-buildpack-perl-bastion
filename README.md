Perl bastion container
======================

Horrible hacks

Publish a new container
-----------------------
```
$ ssh dev.toolforge.org
$ become containers
$ toolforge build start --image-name bastion \
  https://gitlab.wikimedia.org/toolforge-repos/containers-bastion
```

License
-------
Licensed under the [GPL-3.0-or-later][] license. See [COPYING][] for the full
license.

[Build Service]: https://wikitech.wikimedia.org/wiki/Help:Toolforge/Build_Service
[GPL-3.0-or-later]: https://www.gnu.org/licenses/gpl-3.0.html
[COPYING]: COPYING
